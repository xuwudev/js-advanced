// Виведіть цей масив на екран у вигляді списку (тег ul – список має бути згенерований за допомогою Javascript).
// На сторінці повинен знаходитись div з id="root", куди і потрібно буде додати цей список (схоже завдання виконувалось в модулі basic).
// Перед додаванням об'єкта на сторінку потрібно перевірити його на коректність (в об'єкті повинні міститися всі три властивості - author, name, price). Якщо якоїсь із цих властивостей немає, в консолі має висвітитися помилка із зазначенням - якої властивості немає в об'єкті.
// Ті елементи масиву, які не є коректними за умовами попереднього пункту, не повинні з'явитися на сторінці.

const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  },
]

function isValidBook(book) {
  return "author" in book && "name" in book && "price" in book
}

function displayBooks() {
  const rootElement = document.getElementById("root")
  const ulElement = document.createElement("ul")

  books.forEach((book) => {
    try {
      if (isValidBook(book)) {
        const liElement = document.createElement("li")
        liElement.textContent = `${book.author} - ${book.name} - ${
          book.price || "Price not available"
        }`
        ulElement.appendChild(liElement)
      } else {
        throw new Error(`Invalid book object: ${JSON.stringify(book)}`)
      }
    } catch (error) {
      console.error(error.message)
    }
  })

  rootElement.appendChild(ulElement)
}

displayBooks()
