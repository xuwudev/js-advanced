// Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript
// Дочірній елемент наслідує пропертіз батьківського
// Для чого потрібно викликати super() у конструкторі класу-нащадка?
// Щоб працювало наслідування батьківського елемента, так як super викликає конструктор батьківського елемента

class Employee {
  constructor(name, age, salary) {
    this._name = name
    this._age = age
    this._salary = salary
  }

  get name() {
    return this._name
  }

  get age() {
    return this._age
  }

  get salary() {
    return this._salary
  }

  set salary(value) {
    this._salary = value
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary)
    this._lang = lang
  }

  get lang() {
    return this._lang
  }

  set lang(value) {
    this._lang = value
  }

  get salary() {
    return super.salary * 3
  }
}

const programmer1 = new Programmer("Sonya Xuwu", 30, 1000, "JavaScript")
const programmer2 = new Programmer("Ichiro Ivasaki", 25, 100, "Java")
console.log(programmer1)
console.log(programmer2)
