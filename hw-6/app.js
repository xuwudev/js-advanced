document.getElementById("find-button").addEventListener("click", bothData)

async function ipFinder() {
  const response = await fetch("https://api.ipify.org/?format=json")
  const data = await response.json()

  return data.ip
}

async function getAddress(ipAddress) {
  const response = await fetch(
    `http://ip-api.com/json/${ipAddress}?fields=continent,country,regionName,city,district`
  )
  const data = await response.json()

  return data
}

async function bothData() {
  const ipAddress = await ipFinder()
  const addressInfo = await getAddress(ipAddress)

  const infoParagraph = document.getElementById("ip-info")
  infoParagraph.innerHTML = `
        Континент: ${addressInfo.continent},<br> 
        Країна: ${addressInfo.country},<br> 
        Регіон: ${addressInfo.regionName},<br> 
        Місто: ${addressInfo.city},<br> 
        Район: ${addressInfo.district}
        `
}
