async function getUsers() {
  const response = await fetch("https://ajax.test-danit.com/api/json/users")
  const data = await response.json()

  return data
}

async function getPosts() {
  const response = await fetch("https://ajax.test-danit.com/api/json/posts")
  const data = await response.json()

  return data
}

async function createPosts() {
  const [users, posts] = await Promise.all([getUsers(), getPosts()])

  posts.forEach((post) => {
    const user = users.find((user) => user.id === post.userId)
    post.user = user
    new userCard(post)
  })
}

class userCard {
  constructor(post) {
    this.post = post
    this.cardElement = this.createCardElement()
    this.deleteEvent()
  }

  createCardElement() {
    const card = document.createElement("div")
    card.className = "card"

    const user = document.createElement("p")
    user.className = "card-user"
    user.innerHTML = `<h2>${this.post.user.name}</h2>
                      <p>${this.post.user.email}</p>`

    const title = document.createElement("h2")
    title.className = "card-title"
    title.textContent = this.post.title

    const body = document.createElement("p")
    body.textContent = this.post.body

    const deleteButton = document.createElement("button")
    deleteButton.className = "delete-button"
    deleteButton.textContent = "Delete"

    card.appendChild(user)
    card.appendChild(title)
    card.appendChild(body)
    card.appendChild(deleteButton)

    document.getElementById("userstorage").appendChild(card)

    return card
  }

  deleteEvent() {
    this.cardElement
      .querySelector(".delete-button")
      .addEventListener("click", () => {
        this.deleteCard()
      })
  }

  deleteCard() {
    const postId = this.post.id
    fetch(`https://ajax.test-danit.com/api/json/posts/${postId}`, {
      method: "DELETE",
    })
      .then((response) => {
        if (response.ok) {
          this.cardElement.remove()
        } else {
          console.error("Failed to delete post")
        }
      })
      .catch((err) => {
        throw new Error(err.message)
      })
  }
}

createPosts()
