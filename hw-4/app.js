// Надіслати AJAX запит на адресу https://ajax.test-danit.com/api/swapi/films та отримати список усіх фільмів серії Зоряні війни
// Для кожного фільму отримати з сервера список персонажів, які були показані у цьому фільмі. Список персонажів можна отримати з властивості characters.
// Як тільки з сервера буде отримана інформація про фільми, відразу вивести список усіх фільмів на екрані. Необхідно вказати номер епізоду, назву фільму, а також короткий зміст (поля episodeId, name, openingCrawl).
// Як тільки з сервера буде отримано інформацію про персонажів будь-якого фільму, вивести цю інформацію на екран під назвою фільму.

function getCharacters() {
  fetch("https://ajax.test-danit.com/api/swapi/people", {
    method: "GET",
    headers: {
      "Content-type": "application/json",
    },
  })
    .then((characters) => characters.json())
    .then((charactersList) =>
      charactersList.forEach((character) => {
        const characterName = character.name
        const characterId = character.id
        console.log(characterName, characterId)
      })
    )
}

function getFilms() {
  return fetch("https://ajax.test-danit.com/api/swapi/films", {
    method: "GET",
    headers: {
      "Content-type": "application/json",
    },
  })
    .then((films) => films.json())
    .then((filmList) => {
      const root = document.getElementById("root")
      const ul = document.createElement("ul")
      fetch("https://ajax.test-danit.com/api/swapi/characters"),
        {
          method: "GET",
          headers: {
            "Content-type": "application/json",
          },
        }
      filmList.forEach((film) => {
        const li = document.createElement("li")
        li.innerHTML = `
        <br>
        Id: ${film.id}
        <br>
        <br>
        Character: ${film.characters}
        <br>
        <br>
        Episode: ${film.episodeId}
        <br>
        <br>
        OpeningCrawl: ${film.openingCrawl}
        <br>
        `

        ul.appendChild(li)
      })
      root.appendChild(ul)
    })
    .catch((err) => {
      console.log(err)
    })
    .finally(() => {})
}

getFilms()
getCharacters()
