// Запит даних із двох різних API та їх комбінування

// Припустимо, у вас є два різні API, кожен повертає якісь дані.
//     Вам потрібно виконати асинхронні запити до обох API, дочекатися їх завершення,
//     а потім поєднати результати для подальшої обробки.

// https://jsonplaceholder.typicode.com/posts
// https://jsonplaceholder.typicode.com/users

async function getPosts() {
  const response = await fetch("https://jsonplaceholder.typicode.com/posts")
  const postsData = await response.json()
  return postsData
}

async function getUsers() {
  const response = await fetch("https://jsonplaceholder.typicode.com/users")
  const usersData = await response.json()
  return usersData
}

async function combineData() {
  try {
    const [users, posts] = await Promise.all([getUsers(), getPosts()])
    const combineData = {
      users,
      posts,
    }
    console.log(combineData)
  } catch (err) {
    throw new Error(err.message)
  }
}
